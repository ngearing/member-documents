<?php

/**
 * Plugin Name: Member Documents
 * Description: Manage documents and who can see them.
 * Author: Greengraphics
 * Author Uri: https://greengraphics.com.au/
 * Version: 0.0.2
 * Bitbucket Plugin URI: https://bitbucket.org/ngearing/member-documents
 */

function wpse255804_add_page_template( $templates ) {
	$templates['member-documents.php'] = 'Member Documents';
	return $templates;
}
add_filter( 'theme_page_templates', 'wpse255804_add_page_template' );

function load_acf_json( $paths ) {
	$paths[] = plugin_dir_path( __FILE__ ) . '/acf-json';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'load_acf_json' );


function register_assets() {
	wp_register_style( 'documents-css', plugin_dir_url( __FILE__ ) . 'dist/documents.css' );
	wp_register_script( 'documents-js', plugin_dir_url( __FILE__ ) . 'dist/documents.js' );
}
add_action( 'wp_enqueue_scripts', 'register_assets' );

function load_assets( $assets = 'all' ) {
	if ( in_array( $assets, [ 'all', 'css' ], true ) ) {
		wp_enqueue_style( 'documents-css' );
	}

	if ( in_array( $assets, [ 'all', 'js' ], true ) ) {
		wp_enqueue_script( 'documents-js' );
	}
}


function filter_the_content_in_the_main_loop( $content ) {

	// Check if we're inside the main loop in a single post page.
	if ( in_the_loop() && is_main_query() && get_post_meta( get_the_id(), '_wp_page_template', true ) === 'member-documents.php' && user_can_access() ) {
		return $content . display_document_folders();
	} elseif ( in_the_loop() && is_main_query() && get_post_meta( get_the_id(), '_wp_page_template', true ) === 'member-documents.php' ) {
		return show_login_form();
	}

	return $content;
}
add_filter( 'the_content', 'filter_the_content_in_the_main_loop' );


/**
 * Check user access for certain pages.
 */
function user_can_access( $post = null ) {

	if ( ! is_user_logged_in() ) {
		return false;
	}

	// User is Admin or Editor.
	if ( current_user_can( 'edit_others_pages' ) ) {
		return true;
	}

	if ( ! $post ) {
		$post = get_post( get_the_id() );
	}

	if ( get_field( 'who_can_view_this', $post ) === 'All logged in users' ) {
		return true;
	}

	$user = wp_get_current_user();

	// This post has special access field and user is approved.
	$users_who_can = get_field( 'who_can', $post );
	if ( $users_who_can ) {
		$user_ids = wp_list_pluck( $users_who_can, 'ID' );
		if ( in_array( $user->ID, $user_ids, true ) ) {
			return true;
		}
	}

	return false;
}

function show_login_form() {
	global $current_user;

	load_assets( 'css' );

	echo "<div class='document-login-form'>";

	if ( ! is_user_logged_in() ) :
		echo '<h2>Members Login</h2>';
		wp_login_form();
	else :
		echo '<h3>Hello ' . $current_user->user_firstname . '</h3>';

		$document_pages = get_pages(
			[
				'meta_key'   => '_wp_page_template',
				'meta_value' => 'member-documents.php',
			]
		);

		$document_pages = array_filter(
			$document_pages, function( $page ) {
				return user_can_access( $page );
			}
		);

		if ( count( $document_pages ) > 0 ) {
			echo '<p><b>You can access the following member pages:</b></p>';
			echo '<ul>';

			foreach ( $document_pages as $page ) {
					echo "<li><a href='" . get_the_permalink( $page ) . "'>$page->post_title</a></li>";
			}

			echo '</ul>';
		}

		echo "<a href='" . wp_logout_url() . "'>Logout</a>";
	endif;

	echo '</div>';
}

/**
 * This functions displays the document folders.
 */
function display_document_folders() {
	load_assets();

	$post_id = get_the_id();

	$folders = get_field( 'document_folder', $post_id );

	$output = '';

	if ( $folders ) :
		$output .= "<div class='document-folders'>";

		foreach ( $folders as $folder ) {
			if ( ! $folder['documents'] ) {
				continue;
			}

			$output .= "<div class='folder'>";
			$output .= sprintf(
				'<div class="folder-head"><h4 class="folder-title">%s</h4><button class="folder-toggle" aria-label="Show folder"></button></div>',
				$folder['folder_name'] ?: 'Folder'
			);

			$output .= '<div class="folder-content"><ul>';

			foreach ( $folder['documents'] as $document ) {
				$document_file = (object) $document['file'];
				$output .= sprintf(
					'<li>
						<a href="%s" class="document-file" target="_blank">%s</a>
					</li>',
					$document_file->url,
					$document_file->title
				);
			}

			$output .= '</ul></div>';

			$output .= '</div>';
		}

		$output .= '</div>';
	endif;

	return $output;
}
