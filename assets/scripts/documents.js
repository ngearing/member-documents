;(function() {
    'use strict'

    const folders = document.querySelectorAll('.folder')

    for (let i = 0; folders.length > i; i++) {
        let folder = folders[i]
        let folderHead = folder.querySelector('div.folder-head')
        let folderContent = folder.querySelector('div.folder-content')

        folderHead.addEventListener('click', function(e) {
            folder.classList.toggle('active')

            let openFolders = Array.from(folders).filter(elem =>
                elem.classList.contains('active')
            )
            openFolders.map(elem => {
                if (elem !== this.closest('.folder'))
                    elem.classList.remove('active')
            })
        })
    }
})()
